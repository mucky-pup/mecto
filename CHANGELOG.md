# CHANGELOG

## v0.7.2

- Add more specs
- Fix typo in `Mecto.SchemaValidator` moduledoc
- Added note to readme explaining why protocol_ex is required
- Update dependencies

## v0.7.1

- Add licence
- Add links to licence and changelog in docs

## v0.7.0

- Add support for `Ecto.Type`
- Add support for embedded types
- Update docs
- Update dependencies

## v0.6.0

- Fix conversion of module names to snake case

## v0.5.0

- Ensure error return of `validate` is always a list

## v0.4.0

- Add dialyzer and (some) typespecs

## v0.3.0

- Handle :through relationships 

## v0.2.0

- Relax nimble_parsec version requirements 

## v0.1.0

- Initial release
