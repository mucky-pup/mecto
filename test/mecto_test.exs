defmodule MectoTest do
  use ExUnit.Case
  doctest Mecto

  describe "extracting struct fields from markup" do
    test "works on text without any fields" do
      text = "This text has no fields to interpolate"

      assert %{} == Mecto.extract_nodes(text)
    end

    test "can extract fields" do
      text = "[[blog_post.title]], with [[blog_post.author.username.content]]"

      assert %{blog_post: %{title: 1, author: %{username: %{content: 1}}}} =
               Mecto.extract_nodes(text)
    end

    test "extracts children and fields" do
      text = "Post [[blog_post.title]]. Top comment: [[blog_post.comments[0].content]]"

      assert %{blog_post: %{comments: %{0 => %{content: 1}}}} = Mecto.extract_nodes(text)
    end

    test "can merge child lists" do
      text =
        "This post is [[blog_post.comments[0].content]], with [[blog_post.comments[1].content]]"

      assert %{blog_post: %{comments: %{0 => %{content: 1}, 1 => %{content: 1}}}} =
               Mecto.extract_nodes(text)
    end

    test "counts leaf occurence" do
      text =
        "some [[blog_post.title]] with [[blog_post.content]]. Also the same [[blog_post.title]]"

      assert %{blog_post: %{title: 2, content: 1}} = Mecto.extract_nodes(text)
    end

    test "fails if atom doesn't exist" do
      text = "This field [[does.not.exist]]"

      assert {:error,
              "field cannot be converted to a non-existing atom. Maybe your markup has a typo?"} =
               Mecto.extract_nodes(text)
    end

    test "fails on invalid markup" do
      texts = [
        "This has some [[invalid|markup]]",
        "[[[invalid.markup]]",
        "more [[]] invalid markup",
        "invalid [[closing.tag"
      ]

      for text <- texts do
        assert {:error, "invalid markup"} = Mecto.extract_nodes(text)
      end
    end
  end

  describe "validating markup" do
    test "gets root fields" do
      text = "This post is [[blog_post.title]]"

      assert %{blog_post: %{title: :string}} = Mecto.validate(text, Mecto.BlogPost)
    end

    test "gets relationships" do
      text =
        "Authored by [[blog_post.author.name]]. First comment is [[blog_post.comments[0].content]]"

      assert %{blog_post: %{author: %{name: :string}, comments: %{0 => %{content: :string}}}} =
               Mecto.validate(text, Mecto.BlogPost)
    end

    test "fails if root fields are missing" do
      text = "This post is [[blog_post.invalid_field]]"

      assert {:error, ["blog_post.invalid_field does not exist"]} =
               Mecto.validate(text, Mecto.BlogPost)
    end

    test "fails if relationship fields are missing" do
      text = "Authored by [[blog_post.author.content]]"

      assert {:error, ["blog_post.author.content does not exist"]} =
               Mecto.validate(text, Mecto.BlogPost)
    end

    test "multiple failures" do
      text =
        "This post is [[blog_post.invalid_field]] [[blog_post.author.content]] [[blog_post.comments[0].name]]"

      expected_errors = [
        "blog_post.invalid_field does not exist",
        "blog_post.author.content does not exist",
        "blog_post.comments[0].name does not exist"
      ]

      assert {:error, ^expected_errors} = Mecto.validate(text, Mecto.BlogPost)
    end

    test "fails if relationship are invalid" do
      text =
        "Authored by [[blog_post.author[0].name]]. First comment is [[blog_post.comments.content]]"

      expected_errors = [
        "blog_post.comments have a cardinality of :many, but is being used like a single element",
        "blog_post.author has a cardinality of :one, but is being used like a list"
      ]

      assert {:error, ^expected_errors} = Mecto.validate(text, Mecto.BlogPost)
    end

    test "fails if markup is invalid" do
      text = "Some [[invalid markup]]"

      assert {:error, ["invalid markup"]} = Mecto.validate(text, Mecto.BlogPost)
    end
  end

  describe "parsing schema" do
    test "builds map from Ecto schema" do
      assert Mecto.parse_schema(Mecto.BlogPost) == %{
               id: :id,
               title: :string,
               content: :string,
               views: :integer,
               status: :string,
               author_id: :id,
               upvotes: {:many, %{timestamp: :utc_datetime}},
               author: {:one, %{id: :id, name: :string}},
               comments: {:many, %{blog_post_id: :id, title: :string, content: :string}}
             }
    end

    test "fails if module has no schema" do
      assert {:error, :missing_schema} = Mecto.parse_schema(Mecto.Foo)
    end

    test "fails if module doesn't exist" do
      assert {:error, :nofile} = Mecto.parse_schema(Mecto.NoModule)
    end

    test "handles through relationships" do
      assert Mecto.parse_schema(Mecto.Author) ==
               %{
                 id: :id,
                 name: :string,
                 posts:
                   {:many,
                    %{
                      id: :id,
                      title: :string,
                      content: :string,
                      views: :integer,
                      status: :string,
                      author_id: :id,
                      upvotes: {:many, %{timestamp: :utc_datetime}},
                      comments: {:many, %{blog_post_id: :id, title: :string, content: :string}}
                    }},
                 comments: {:many, %{blog_post_id: :id, title: :string, content: :string}}
               }
    end
  end

  describe "interpolating" do
    test "works with no fields" do
      text = "This text has no fields to interpolate"

      assert {:ok, ^text} = Mecto.interpolate(text, %Mecto.BlogPost{})
    end

    test "works with basic fields" do
      text = "A [[blog_post.title]]."

      assert {:ok, "A some post."} = Mecto.interpolate(text, %Mecto.BlogPost{title: "some post"})
    end

    test "works with children" do
      text =
        "Top comment: [[blog_post.comments[1].content]]. Written by: [[blog_post.author.name]]."

      expected_text = "Top comment: this was interpolated. Written by: me."

      data = %Mecto.BlogPost{
        author: %Mecto.Author{name: "me"},
        comments: [
          %Mecto.Comment{},
          %Mecto.Comment{content: "this was interpolated"}
        ]
      }

      assert {:ok, ^expected_text} = Mecto.interpolate(text, data)
    end

    test "doesn't work with invalid markup" do
      text = "Some [[]] invalid markup"

      assert {:error, "invalid markup"} = Mecto.interpolate(text, %Mecto.BlogPost{})
    end

    test "doesn't interpolate invalid fields" do
      text = "Some [[blog_post.invalid_field]]"

      assert {:ok, "Some "} = Mecto.interpolate(text, %Mecto.BlogPost{})
    end

    test "interpolates for custom Ecto types" do
      text = "[[blog_post.status]]"

      assert {:ok, "draft"} = Mecto.interpolate(text, %Mecto.BlogPost{status: :draft})
    end
  end
end
