defmodule Mecto.Author do
  @moduledoc false

  use Ecto.Schema

  schema "authors" do
    field(:name, :string)

    has_many(:posts, Mecto.BlogPost)
    has_many(:comments, through: [:posts, :comments])
  end
end
