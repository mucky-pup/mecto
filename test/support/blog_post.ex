defmodule Mecto.BlogPost do
  @moduledoc false

  use Ecto.Schema

  schema "posts" do
    field(:title, :string)
    field(:content, :string)
    field(:views, :integer)
    field(:status, Ecto.Enum, values: [:draft, :published])
    embeds_many(:upvotes, Mecto.Upvote)

    belongs_to(:author, Mecto.Author)
    has_many(:comments, Mecto.Comment)
  end
end
