defmodule Mecto.Upvote do
  @moduledoc false

  use Ecto.Schema

  @primary_key false
  embedded_schema do
    field(:timestamp, :utc_datetime)
  end
end
