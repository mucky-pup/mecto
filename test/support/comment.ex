defmodule Mecto.Comment do
  @moduledoc false

  use Ecto.Schema

  @primary_key false
  schema "posts" do
    field(:title, :string)
    field(:content, :string)

    belongs_to(:blog_post, Mecto.BlogPost)
  end
end
