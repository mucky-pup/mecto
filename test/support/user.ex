defmodule Mecto.User do
  @moduledoc false

  use Ecto.Schema

  schema "users" do
    field(:username, :string)
  end
end
