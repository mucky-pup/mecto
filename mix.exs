defmodule Mecto.MixProject do
  use Mix.Project

  @version "0.7.2"
  @source_url "https://gitlab.com/mucky-pup/mecto"

  def project do
    [
      app: :mecto,
      version: @version,
      elixir: "~> 1.12",
      start_permanent: Mix.env() == :prod,
      elixirc_paths: elixirc_paths(Mix.env()),
      source_url: @source_url,
      test_coverage: [tool: ExCoveralls],
      preferred_cli_env: [
        coveralls: :test,
        "coveralls.detail": :test,
        "coveralls.html": :test,
        "coveralls.multiple": :test
      ],
      compilers: Mix.compilers() ++ [:protocol_ex],
      description: description(),
      docs: docs(),
      package: package(),
      deps: deps(),
      dialyzer: [
        plt_core_path: "priv/plts",
        plt_file: {:no_warn, "priv/plts/dialyzer.plt"},
        plt_add_apps: [:mix],
        ignore_warnings: ".dialyzer_ignore.exs"
      ]
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:nimble_parsec, "~> 1.2.3 or ~> 1.3"},
      {:excoveralls, "~> 0.18.0", only: :test},
      {:ex_doc, ">= 0.0.0", only: [:dev, :test], runtime: false},
      {:inch_ex, github: "tshakah/inch_ex", only: [:dev, :test]},
      {:ecto, "~> 3.10", only: :test},
      {:dialyxir, "~> 1.1", only: [:dev, :test], runtime: false},
      {:protocol_ex, "~> 0.4.0"}
    ]
  end

  defp description do
    "'Mail merging' with Ecto structs. A parser to interpolate MediaWiki-like `[[foo.bar]]` markup using data from Ecto schemas."
  end

  defp docs do
    [
      main: "readme",
      source_ref: "v#{@version}",
      source_url: @source_url,
      extras: ["README.md", "CHANGELOG.md"]
    ]
  end

  defp package do
    [
      licenses: ["CC-BY-SA-4.0"],
      links: %{"GitLab" => @source_url, "Changelog" => "https://hexdocs.pm/mecto/changelog.html"},
      files: ~w(CHANGELOG.md lib LICENSE mix.exs README.md)
    ]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]
end
