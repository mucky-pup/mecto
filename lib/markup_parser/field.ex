defmodule Mecto.MarkupParser.Field do
  @moduledoc false

  import NimbleParsec

  @dialyzer :no_opaque

  defp identifier(), do: ascii_string([?a..?z, ?A..?Z, ?0..?9, ?_], min: 1)

  def bracket_access do
    ignore(string("["))
    |> concat(integer(min: 1))
    |> ignore(string("]"))
  end

  def dot_access do
    ignore(string("."))
    |> concat(identifier())
  end

  def field do
    identifier()
    |> repeat(choice([dot_access(), bracket_access()]))
  end
end
