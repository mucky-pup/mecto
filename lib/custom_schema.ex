import ProtocolEx

defprotocol_ex Mecto.CustomSchema do
  @moduledoc """
  The `protocol_ex` protocol used to create schemas for custom Ecto types.
  """

  def type(module, data)
end

defimpl_ex Ecto.Enum, Ecto.Enum, for: Mecto.CustomSchema do
  @moduledoc """
  Implementation of `Mecto.CustomSchema` for `Ecto.Enum`, using `protocol_ex`.
  """

  # Note that the module is passed in as a first argument if required, although
  # it's not needed for `Ecto.Enum`.
  @spec type(module(), map()) :: Enumerable.t()
  def type(_module, %{type: type}), do: type
end
